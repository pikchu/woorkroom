import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      redirect: "/login",
    },
    {
      path: "/login",
      component: () => import("@/views/logIn/Index.vue"),
    },
    {
      path: "/signin",
      component: () => import("@/views/signIn/Index.vue"),
      children: [
        {
          path: "/signIn/signInrig",
          component: () => import("@/views/signIn/signInrig.vue"),
        },
        {
          path: "/signin/loginme",
          component: () => import("@/views/signIn/loginme.vue"),
        },
        {
          path: "/signin/logingsgk",
          component: () => import("@/views/signIn/logingsgk.vue"),
        },
        {
          path: "/signin/logintime",
          component: () => import("@/views/signIn/logintime.vue"),
        },
        {
          path: "/signin/logingo",
          component: () => import("@/views/signIn/logingo.vue"),
        },
      ],
    },
    {
      path: "/home",
      component: () => import("@/views/home/Index.vue"),
      children: [
        //仪表盘
        {
          path: "/home/instrument",
          component: () => import("@/views/home/instrument/Index.vue"),
        },
        //项目
        {
          path: "/home/undertaking",
          component: () => import("@/views/home/undertaking/Index.vue"),
        },
        //日历
        {
          path: "/home/calendar",
          component: () => import("@/views/home/calendar/Index.vue"),
        },
        //假期
        {
          path: "/home/holiday",
          component: () => import("@/views/home/holiday/Index.vue"),
        },
        //雇员
        {
          path: "/home/servant",
          component: () => import("@/views/home/servant/Index.vue"),
        },
        //消息
        {
          path: "/home/message",
          component: () => import("@/views/home/message/Index.vue"),
        },
        //门户网站
        {
          path: "/home/gateway",
          component: () => import("@/views/home/gateway/Index.vue"),
        },
      ],
    },
  ],
});

export default router
